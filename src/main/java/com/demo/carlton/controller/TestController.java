package com.demo.carlton.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.carlton.entity.FeignBean;
import com.demo.carlton.entity.User;
import com.demo.carlton.feign.TestFeign;

import feign.Feign;
import feign.Feign.Builder;
import feign.Request.Options;
import feign.Retryer;
import feign.auth.BasicAuthRequestInterceptor;
import feign.codec.Decoder;
import feign.codec.Encoder;

/**
* 
* <p>Title: TestController.java</p>
* @author Carlton
* @date 2019年4月29日 上午9:38:01
*/
@RestController
// 【1】import FeignClientsConfiguration.class
@Import(FeignClientsConfiguration.class)
public class TestController {

	// 不需要 @Autowired
	TestFeign testFeign;

	// 【2】 构造函数添加 @Autowired ，注入encoder，decoder，构建 TestFeign
	@Autowired
	public TestController(FeignBean feignBean, Encoder encoder, Decoder decoder) {
		// options方法指定连接超时时长及响应超时时长，retryer方法指定重试策略
		Builder builder = Feign.builder();
		// 设置http basic验证
		builder = builder.contract(new feign.Contract.Default()).requestInterceptor(
				new BasicAuthRequestInterceptor(feignBean.getAdminName(), feignBean.getAdminPassword()));
		// 【3】设置编码，不然会报错feign.codec.EncodeException
		builder = builder.encoder(encoder).decoder(decoder);
		// options方法指定连接超时时长及响应超时时长，retryer方法指定重试策略
		builder = builder.options(new Options(feignBean.getOpion_conn(), feignBean.getOpion_read()))
				.retryer(new Retryer.Default(feignBean.getRetry_period(), feignBean.getRetry_maxPeriod(),
						feignBean.getRetry_maxAttempts()));
		// 【4】 target 链接目标feing，并指定访问域名
		testFeign = builder.target(TestFeign.class, feignBean.getUrl());
//		testFeign = builder.target(Target.EmptyTarget.create(TestFeign.class));
		System.out.println(000);
	}

	@PostMapping("/getUser")
	public User getUser(@RequestParam(defaultValue = "1") int id) {
		System.out.println("id=" + id);
		User user = testFeign.getUser(id);
		return user;
	}

	@PostMapping("/getUser2")
	public User getUser2(@RequestParam(defaultValue = "1") int id) {
		System.out.println("id=" + id);
		User user = new User(id, "my name " + id, (id * 10));
		return user;
	}

}
