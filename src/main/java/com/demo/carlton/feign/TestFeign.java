package com.demo.carlton.feign;

import java.net.URI;

import org.springframework.cloud.openfeign.FeignClient;
import com.demo.carlton.entity.User;

import feign.Param;
import feign.RequestLine;

/**
* 
* <p>Title: TestFeign.java</p>
* @author Carlton
* @date 2019年4月29日 上午9:26:36
*/
@FeignClient(name = "just-a-name")
public interface TestFeign {

	@RequestLine("POST /getUser2?id={id}")
	public User getUser(@Param("id") int id);

	@RequestLine("POST /getUser2?id={id}")
	public User getUser(URI uri, @Param("id") int id);

}
